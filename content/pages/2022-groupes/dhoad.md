title: DHOAD : les gitans du Rajasthan 2022
template: groupe

## Samedi 17 Sept. 22h30 – Parc de la Cerisaie
![Los Piratas]({static}/images/2022-groupes/dhoad.jpg)


Ce concert vous est offert par l’ambassade de l’Inde à Paris, dans le cadre de la célébration des 75 ans de l’Indépendance de l’Inde en 2022.

<div class="mini">
<img src="{static}/images/partenaires/logo4-1.jpg" />
<img src="{static}/images/partenaires/002-2.jpg" />
<img src="{static}/images/partenaires/namaste-france-2-849x1024.jpg" />
</div>

VENU DU RAJASTHAN, HÉRITIER D’UNE ILLUSTRE FAMILLE DE MUSICIENS DES MAHARAJAS, « LE PETIT PRINCE » RAHIS BHARTI EST À LA TÊTE DE LA PROGRAMMATION DHOAD GYPSIES OF RAJASTHAN.

Il a trainé dans ses bagages son tabla et toutes les couleurs de son pays. Dans leurs mots, dans les chants, ce sont les princesses hindoues qui attendent la pluie.

L’amer souvenir des déserts antiques, les illustres palais des rois d’autrefois et toujours l’appel langoureux de l’être aimé.

Musiciens, chanteurs et danseuses vous emportent dans un intermède bouillonnant hors du temps et des frontières.

Derrière eux, c’est toute l’histoire culturelle indienne qui se révèle, une tradition orale transmise depuis sept générations. Aujourd’hui, Rahis Bharti est à la tête d’un empire.

Plus d’un millier de concerts autour du monde on fait de Dhoad, les gitans du Rajasthan, l’ambassadeur culturel du Rajasthan

<div id="video_wrap"><iframe title="L'histoire de Dhoad, les gitans du Rajasthan" src="https://www.youtube.com/embed/kClXvgpCDt8?start=25&amp;feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
