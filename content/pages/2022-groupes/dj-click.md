title: DJ Click 2022
template: groupe


## Samedi 17 Sept. 0h00 Parc de la Cerisaie
![Los Piratas]({static}/images/2022-groupes/dj-click.jpg)

Depuis quelques années en collaboration avec le Gypsy Lyon Festival, Dj Click programme ces nuits gypsy qui nous font bouger hors de nos frontières. Il rend fou les douaniers.

On l’imagine aux confins du Rajasthan avec des joueurs de tablas ; il trace la route avec des musiciens tziganes du coté de Bucharest. On le perd a Essaouira sur un airde gnawa ; on le retrouve a Sevilla en pleine fiesta gitana.

Qu’il soit seul ou accompagné de ses musiciens, qu’il mixe dans les soirées enfiévrées ou pour les gamins des quartiers, Dj Click reste un musicien profondément attachant et passionnant.

Sollicité aux quatre coins de la planète, Click continue ainsi son aventure sur les routes, explorant les richesses de toutes les cultures rencontrées, les remixant, triturant, bidouillant pour surtout nous faire rêver.


<div id="video_wrap"><iframe title="Dj Click (2013)" src="https://www.youtube.com/embed/MrCHpCte60Y?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
