title: Fat Bastard Gang Band 2022
template: groupe

## Vendredi 16 Sept. 22h30 – Parc de la Cerisaie

![Fat Bastard Gang Band]({static}/images/2022-groupes/fat-bastard-gang-band.jpg)


Depuis sa création en 2009, le Fat s’inscrit dans une démarche artistique forte : le métissage !

Le nom de cette formation est déjà en soit une affirmation de ce mélange des cultures. Ils puisent leurs influences autant dans les musiques traditionnelles balkaniques que dans les sons électroniques actuels et chantent dans différentes langues.

Une ballade survoltée qui nous promène, nous guide entre salsa, groove nord américain et musiques du Maghreb, chants traditionnels des Balkans et punk.

Cette équipe de sept musiciens, invite à un voyage à bord de leur vaisseau construit de folie, d’amour et spiritualité. 

<div id="video_wrap"><iframe title="MASALA Tour : The FAT BASTARD GB official" src="https://www.youtube.com/embed/Qsopg_cLw5s?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
