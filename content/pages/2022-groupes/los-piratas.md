title: Los Piratas 2022
template: groupe

## Vendredi 16 Sept. 19h30 – Parc de la Cerisaie

![Los Piratas]({static}/images/2022-groupes/los-piratas.jpg)

Au cours d’un voyage initiatique au cœur de l’Andalousie, le groupe a été baptisé Los Piratas par des aficionados.

Fondée en 2010 cette formation évolue aujourd’hui sous la forme d’un collectif d’artistes.

Partis simplement avec leurs guitares, ils ont navigué au gré des rencontres, de Córdoba à Séville, de Jerez aux Saintes-Maries. Lors de cette aventure, ils se sont imprégnés de chacune des notes et de chacun des chants délivrés par les joueurs de flamenco croisés sur leur route. Une expérience musicale unique…

De retour à Lyon, les pirates ont déballé leur trésor gipsy-latino : une fusion explosive de flamenco traditionnel, de rumba et de musiques latines. Ce mélange est envoyé harmonieusement par les voix, les guitares et les percussions.

Un show festif, chaleureux et émouvant, à l’image du flamenco, qui emporte le public dans l’effervescence des nuits latines !

Los Piratas propagent, sous des vagues rythmées et festives, le feu sacré du flamenco.

<div id="video_wrap"><iframe title="LOS PIRATAS - MEDLEY RUMBA 1" src="https://www.youtube.com/embed/aFuDWB0t-Uw?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
