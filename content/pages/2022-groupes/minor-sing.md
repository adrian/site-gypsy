title: Minor Sing 2022
template: groupe

## Samedi 17 Sept. 19h30 – Parc de la Cerisaie
![Minor Sing]({static}/images/2022-groupes/minor-sing.jpg)

Voilà 10 ans qu’ils tournent dans les plus beaux festivals de France et d’Europe, de Jazz à Vienne aux Djangofolllies en Belgique, en passant par le Petit Journal Montparnasse, Berlin, Genève, Prague ou Zurich…

Yannick Alcocer, Jean Lardanchet, Sylvain Pourrat et Laurent Vincenza s’accordent avec minutie et sensibilité.

Django Reinhardt et Stéphane Grappelli veillent sur eux, mais leurs interprétations et compositions regardent vers l’avant, avec un modernisme teinté parfois de pop ou de rock.

L’humour est leur marque de fabrique et n’enlève rien à leur finesse de grands musiciens.

<div id="video_wrap"><iframe title="Minor Sing - Joseph Joseph - Gipsy Jazz" src="https://www.youtube.com/embed/frzq43SHJ6c?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
