title: O Deves 2022
template: groupe

## Samedi 17 Sept. 21h – Parc de la Cerisaie
![O DEVES]({static}/images/2022-groupes/odeves.jpg)

O Deves vous présentera des chants tziganes!
Pas du klezmer, pas de la chanson française, pas des compositions vaguement inspirées des musiques de l’Est. Du tzigane ! Larmoyant et envoutant, plaintif et endiablé, nostalgique et enragé, à se faire péter les cordes vocales, à danser toute la nuit, à pleurer comme une madeleine une bouteille de vodka à la main… 1h30 de chants des tziganes de tous pays. Quelques airs connus ainsi que de nombreuses perles rares du répertoire.

Aven amensa !


<div id="video_wrap"><iframe title="O Deves - Tutti Frutti" src="https://www.youtube.com/embed/72AVHNmTnSE?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
