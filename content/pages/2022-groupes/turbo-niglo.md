title: Turbo Niglo 2022
template: groupe

## Vendredi 16 Sept. 21h – Parc de la Cerisaie

![Turbo Niglo]({static}/images/2022-groupes/turbo-niglo.jpg)
Turbo Niglo, une expérience musicale dynamique et festive, une bougie d’allumage qui propulse les délires déjantés et planants des deux guitaristes.

Adeptes du « Do It Yourself », Sami Chaibi et Mike Davis font tout eux-mêmes, de la composition à la mise en scène, ils contrôlent les jeux de lumière, les vidéos et leurs effets scéniques, ce qui leur permet d’aborder leurs univers musical avec une liberté artistique totale et sans concession.


<div id="video_wrap"><iframe title="Turbo Niglo | Invité Vedette" src="https://www.youtube.com/embed/DqTTJP6i124?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" id="fitvid0" frameborder="0"></iframe></div>
