title: Mentions légales
slug: legal

## DROITS

Les images publiées sur le site www.gypsylyonfestival.com  ne sont pas libres de droit et sont protégées par le droit d’auteur et le Code de la Propriété Intellectuelle, conformément à la loi du 1er juillet 1992. Toute reproduction, publication, impression ou utilisation (et notamment sur un site internet), totale ou partielle, est soumise à l’autorisation écrite et préalable du Photographe/du Graphiste et/ou ses ayants droits.

 
## CREDITS PHOTO

David Strickler
Bastien

 
## CREDITS GRAPHISME

Quentin Laignel

 
## ÉDITEUR DU SITE

Ce site est édité par l’association  Les canuts des canits de type loi 1901 déclarée à la préfecture du Rhône sous les numéros :
SIRET : 518434 584 000 18
W691071956.

 
## DIRECTEUR DE LA PUBLICATION

Grégory Dumont (Président).
Les Canuts des canits
Association loi 1901
28, Rue Denfert Rochereau – 69004 LYON.

 
## HÉBERGEUR DU SITE

L’hébergement est assuré par [Jean-Cloud](https://jean-cloud.net).

