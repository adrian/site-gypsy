title: Devenir Partenaire
date: 2


Un partenariat est une association entre deux ou plusieurs entreprises ou entités qui décident de coopérer en vue de réaliser un objectif commun.

Le partenariat peut être de nature économique, financière, scientifique, culturelle, artistique, etc. Tout en laissant leur autonomie à chacun des partenaires, il permet de créer des synergies, de tirer profit d’une complémentarité, de mettre en commun des ressources, d’affronter en commun une situation…

La relation entre les partenaires est formalisée par un contrat ou un protocole de collaboration dans lequel les responsabilités, rôles et contributions financières de chacune des parties sont clairement définis.

## Pour quels types d’action ?

- Mise en avant de votre institution.
- Visibilité dans les médias.
- Insertion au sein des divers supports de communication.

Ainsi, l’association Les Canuts des Canits, s’engage à mettre en avant, tout au long du festival et lors des soirées organisées pour celui-ci, les supports de communication que le partenaire lui aura fournis (affiche, banderole, flyer, etc…).

L’association s’engage également à intégrer le logo du partenaire sur les différents supports de communication dédiés au GYPSY LYON FESTIVAL. Les Canuts des Canits et le partenaire s’engagent à permettre le lien entre leur site respectif.

## Les avantages

- Valoriser l’image de la structure en étant associé à un événement culturel local.
- Renforcer et pérenniser votre visibilité au sein de l’agglomération.
- Une déduction fiscale pouvant aller jusqu’à 60%.

LE GYPSY LYON FESTIVAL est un lieu de rencontre, d’échange et de découverte tourné vers la culture Tsigane, pour que chacun puisse enfin mieux comprendre l’autre dans ses différences. Sa gratuité lui permet de drainer chaque année un nombre croissant de spectateurs dans les lieux incontournables de la Croix-Rousse (+ de 10000 personnes) et ainsi de fédérer l’esprit de ce quartier unique.

