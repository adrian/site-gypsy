title: Ils nous soutiennent déjà
date: 1
template: galerie
 
[![Ville de Lyon]({static}/images/partenaires/Mairie-centrale-300x300.png)](https://www.lyon.fr/ "Ville de Lyon")
[![Mairie Lyon 4]({static}/images/partenaires/Mairie_Croix_Rousse-300x300.jpg)](https://mairie4.lyon.fr/ "Mairie Lyon 4")
[![Métropole de Lyon]({static}/images/partenaires/Grand-Lyon-300x98.jpg)](https://www.grandlyon.com/ "Métropole de Lyon")
[![Savarez]({static}/images/partenaires/savarez-300x185.jpg)](https://www.savarez.fr/ "Savarez")
[![Le Comoedia]({static}/images/partenaires/logo-comœdia-300x120.jpg)](https://www.cinema-comoedia.com/ "Le Comoedia")
[![Lyon Ville Equitable et Durable]({static}/images/partenaires/label-equitabledura-noir-500x310.jpg)](http://cartes.lyon.fr/lved/ "Lyon Ville Equitable et Durable")
[![Lou Rugby]({static}/images/partenaires/logo-lou-2009-ph-300x300.jpg)](http://www.lourugby.fr/ "Lou Rugby")
[![Crédit Mutuel Lyon Croix-Rousse]({static}/images/partenaires/Credit-MUT-Lyon-Croix-Rousse-300x123.png)](https://www.creditmutuel.fr/fr/banques/contact/Details.aspx?banque=10278&guichet=07318&bureau=00&pva=000&type=branch&loca=LYON "Crédit Mutuel Lyon Croix-Rousse")
[![namaste france (2)]({static}/images/partenaires/namaste-france-2-849x1024.jpg)]({static}/images/partenaires/namaste-france-2-849x1024.jpg "namaste france (2)"){.tile-inner .ftg-lightbox}
[![logo4 (1)]({static}/images/partenaires/logo4-1.jpg)]({static}/images/partenaires/logo4-1.jpg "logo4 (1)"){.tile-inner .ftg-lightbox}
[![002 (2)]({static}/images/partenaires/002-2.jpg)]({static}/images/partenaires/002-2.jpg "002 (2)"){.tile-inner .ftg-lightbox}
[![22\_WEB\_BIERE\_DULION]({static}/images/partenaires/22_WEB_BIERE_DULION.png)]({static}/images/partenaires/22_WEB_BIERE_DULION.png "22_WEB_BIERE_DULION"){.tile-inner .ftg-lightbox}
[![22\_WEB\_VILLA\_GILLET]({static}/images/partenaires/22_WEB_VILLA_GILLET.jpg)]({static}/images/partenaires/22_WEB_VILLA_GILLET.jpg "22_WEB_VILLA_GILLET"){.tile-inner .ftg-lightbox}
[![LOGO\_U-Express\_X\_rousse]({static}/images/partenaires/LOGO_U-Express_X_rousse-1024x320.jpg)]({static}/images/partenaires/LOGO_U-Express_X_rousse-1024x320.jpg "LOGO_U-Express_X_rousse"){.tile-inner .ftg-lightbox}
[![argentine web]({static}/images/partenaires/argentine-web.png)]({static}/images/partenaires/argentine-web.png "argentine web"){.tile-inner .ftg-lightbox}
[![kanopee web]({static}/images/partenaires/kanopee-web.png)]({static}/images/partenaires/kanopee-web.png "kanopee web"){.tile-inner .ftg-lightbox}
[![Logo-CAPSAO-contourblanc-couleur-WEB-FT]({static}/images/partenaires/Logo-CAPSAO-contourblanc-couleur-WEB-FT-520x341.png)]({static}/images/partenaires/Logo-CAPSAO-contourblanc-couleur-WEB-FT.png "Logo-CAPSAO-contourblanc-couleur-WEB-FT"){.tile-inner .ftg-lightbox} 
