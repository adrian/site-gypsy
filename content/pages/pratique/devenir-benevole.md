title: Devenir Bénévole
template: programmation
date: 3


[![]({static}/images/pratique/benevoles.jpg)]( "Les bénévoles")
[![]({static}/images/pratique/bar.jpg)]( "Au bar")
[![]({static}/images/pratique/grande_scene.jpg)]( "Sur la grande scène")
[![]({static}/images/pratique/bar.jpg)]( "Et encore au bar")

## Devenir bénévole du Gypsy Lyon Festival, c’est un engagement citoyen d’envergure !

Le Gypsy Lyon Festival doit avant tout être considéré comme un rassemblement citoyen et éco-responsable. A ce titre, l’équipe bénévole organisatrice du festival souhaiterait pouvoir fédérer une vaste palette d’individus autour de son action. Nous encourageons donc tous ceux qui le désirent, à s’impliquer, à nos côtés, dans la vie de la cité, afin d’en devenir ou d’en demeurer acteurs.

Puisque l’accès à la culture constitue l’un des droits les plus fondamentaux dont l’individu doit pouvoir jouir librement, luttons ensemble pour maintenir cette gratuité d’accès aux concerts et autres manifestations, instaurée en 2010, à l’occasion de la première édition. Pour y parvenir, nous avons besoin, au-delà du soutien de nouveaux partenaires, de nouvelles forces vives ! Alors rejoignez-nous, sans plus attendre… Devenez bénévoles !

## Être bénévole du Gypsy Lyon Festival, c’est…

• Faire battre le cœur de sa ville et de son quartier, au rythme d’un événement culturel.

• Participer à la transmission d’un patrimoine humain universel qui tend aujourd’hui à disparaitre : celui de la culture Tsigane.

• S’enrichir de nouvelles expériences humaines en bénéficiant d’un moment de rencontre et d’échange privilégié avec les membres de l’équipe, les artistes et le public.

• S’enrichir et se nourrir, au fil des éditions, de nouvelles expériences musicales et artistiques.

Pour cela, rien de plus simple ! Contactez-nous en nous manifestant vos désirs, de manière à être affectés, dans la mesure du possible et en fonction des besoins du bord, à un poste qui vous convient, dans le domaine qui vous correspond.

Une adresse mail : gestion.benevoles@gypsylyonfestival.com

## Nos besoins

- Bar
- Accueil public
- Accueil artistes
- Catering
- Technique, logistique
- Communication
- Sécurité – secourisme

## Nous offrons

- Le tee-shirt du festival
- Repas et boissons bien sûr pris en charge
- Possibilité d’hébergement

