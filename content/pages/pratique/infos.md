title: Infos par ci par là…
date: 1

## Le Gypsy, c’est où ?

« Le Gypsy Lyon Festival, c’est le festival dans le parc de la Croix-Rousse avec la belle maison bourgeoise !! ».  C’est le Parc de la Cerisaie, qui abrite la Villa Gillet, au 25 rue Chazière, Croix-Rousse
## Consignes de sécurité

En raison des nouvelles mesures de sécurité auxquelles nous sommes tenus de répondre, une fouille aléatoire sera réalisée à l’entrée par des agents de sécurité du festival et l’ouverture des sacs sera obligatoire pour pouvoir pénétrer dans l’enceinte du parc. Merci, par avance, de votre coopération et de votre compréhension.

Sachez par ailleurs que vous ne pourrez pas entrer sur le site en présence de :

- Sacs d’une capacité supérieure à 30L (tous types de sacs, y compris les sacs à dos)
- Objets coupants de toute nature (couteau et autres)
- Bouteilles / bocaux et autres objets en verre
- Canettes en métal
- Animaux (y compris s’ils sont en laisse)

Attention aux vélos !
Les personnes qui viennent à vélo doivent le déposer à l’entrée, contre les barrières Vauban.

Pas de garde des objets personnels !
Nous ne gardons pas à l’accueil les objets personnels des festivaliers qui sont pourtant nombreux à nous le demander (même s’il s’agit d’un excellent pâté ! Si, si, ça nous est déjà arrivé !)
## Tarification

L’accès aux concerts se fait sur le principe de la participation libre. La collecte s’effectue au chapeau sur la Place Commandant Arnaud (passage des bénévoles au sein du public) et à l’entrée, au niveau du stand d’accueil, au Parc de la Cerisaie.

## Handicap moteur

Le site du Parc de la Cerisaie est de plain-pied donc l’ensemble des espaces sont accessibles (sauf la fosse de la grande scène pour les concerts et le bar qui se trouve à côté de la scène).
Un espace est réservé aux personnes en fauteuil et accompagnants au pied de la Villa Gillet, en face de la grande scène (suivre la pancarte « espace PMR »).
Les sanitaires sont accessibles aux personnes en fauteuil roulant
Une place de stationnement est mise à disposition devant l’entrée du Parc de la Cerisaie.

## Enfants et familles

Parents inquiets, rassurez-vous : le site du Parc de la Cerisaie est un espace clos et est constitué de vastes parcelles de pelouse ! Aucun enfant seul ne sera admis dans l’enceinte du festival, ni ne pourra sortir en l’absence d’adulte accompagnant.
Nous disposons au stand d’entrée d’un matelas à langer que nous prêtons aux personnes qui en auraient besoin. Il est à ramener à l’accueil après utilisation.

## Respect de l’environnement

Des dizaines de bacs de tri seront disposés sur l’ensemble du site du Parc de la Cerisaie. Nous comptons donc sur vous pour nous soutenir dans cette démarche en veillant à ne pas laisser de déchets après votre passage et à les déposer dans le bac correspondant.

Petit guide à usage des « trieurs » !
Les bouteilles et flacons en plastique, les emballages en carton, les papiers et journaux et les emballages métalliques sont à déposer dans les bacs de tri (verts à couvercles jaunes). Les barquettes alimentaires, les sacs plastique ainsi que tous les autres déchets sont à mettre dans les autres bacs (verts à couvercles verts). En cas de doute, déposez le déchet en question dans ces mêmes bacs (verts à couverts verts).
Retrouvez toutes les consignes de tri sur le site du Grand Lyon : www.grandlyon.com/services/les-consignes-de-tri-des-dechets.html

## Restauration

Des food-trucks (alimentation biologique et équitable) seront présents sur le site du festival pour que le public puisse se ravitailler. Bon à savoir : on peut régler ses consommations par carte bancaire à partir de 1 € dans les deux buvettes gérées par l’Association.

## Objets trouvés

Les objets trouvés sur le site du Parc de la Cerisaie seront collectés et restitués à leurs propriétaires au niveau du stand d’accueil du festival.

## Premiers secours

Un dispositif de premiers secours sera mis en place au sein du Parc de la Cerisaie pendant toute la durée du festival. Des secouristes seront à votre disposition dans un camion « poste de secours » au pied de la Villa Gillet.

