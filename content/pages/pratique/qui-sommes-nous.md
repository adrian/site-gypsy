title: Qui sommes-nous ?
date: 2

L’association LES CANUTS DES CANITS organise depuis 10 ans le GYPSY LYON FESTIVAL, événement artistique pluridisciplinaire gratuit, social et éco-responsable, qui rassemble un public issu essentiellement de Lyon et de la région. Impliquant artistes et spectateurs variés, établissements d’enseignements et acteurs de la vie locale, il invite à découvrir la culture tzigane et nomade, pour favoriser la reconnaissance de l’égale dignité des cultures et le mieux vivre ensemble. Le festival offre 4 soirées de concert sur 2 scènes plein air avec plus de 15 groupes d’artistes, et un OFF. Il est entièrement géré par des bénévoles, qui s’entourent de professionnels de la technique et de la sécurité pour le bon déroulement des soirées.
## Le Gypsy, c’est quoi ?

Cette 10ème édition renouvelle l’esprit festif, de découverte, social et éco-responsable des années précédentes et reste gratuit, mais souhaite encore une fois innover et étendre son rayonnement avec :

– une nouvelle programmation artistique qui mêle sonorités actuelles et traditionnelles, musique et théâtre, et une soirée carte blanche laissée à DJ Click

– de nouvelles disciplines artistiques pour le OFF, telles que la danse (stage de flamenco), de nouveaux ateliers pour enfants etc., pour favoriser de nouvelles rencontres de publics ; le tout avec un grand nombre d’artistes de la région,

– De nouveaux projets de co-construction locale, notamment avec des établissements d’enseignement artistiques de Lyon 4, l’élargissement du partenariat Emmaüs, de nouvelles associations dans le « village gypsy » en plus du Cinéma Comoedia et de l’Epicerie Séquentielle, pour favoriser les échanges et l’ancrage sur le territoire, et contribuer à la politique culturelle de la Ville de Lyon

– De nouveaux projets éco-responsables pour sensibiliser encore plus le public au respect du territoire: optimisation de l’accessibilité, food trucks bio/de produits locaux en plus des habituels toilettes sèches, gobelets réutilisables, et tri des déchets.

– 100 bénévoles issus de la Métropole pendant le festival, qui apprécient l’ambiance festive, l’humanité et le professionnalisme du festival.

Enfin, comme pour les éditions précédentes, l’association souhaite assurer une gestion financière saine de l’événement en favorisant au mieux les ressources propres, en comptant sur le soutien de partenaires publics pour qui le festival a du sens, et en renforçant la recherche de nouveaux partenaires privés.

