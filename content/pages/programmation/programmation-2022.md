title: Programmation 2022
date: 2022
template: programmation

## Vendredi 16 septembre
[![Les membres du groupe bien habillés avec leur violoncelle]({static}/images/2022-groupes/los-piratas.jpg)]({filename}../2022-groupes/los-piratas.md "Los Piratas")
[![Les têtes joyeuses des membres du groupe]({static}/images/2022-groupes/fat-bastard-gang-band.jpg)]({filename}../2022-groupes/fat-bastard-gang-band.md "Fat Bastard Gang Band")
[![Les deux membres du groupe en position de défi]({static}/images/2022-groupes/turbo-niglo.jpg)]({filename}../2022-groupes/turbo-niglo.md "Turbo Niglo")

## Samedi 17 septembre
[![Le groupe bien habillé avec leurs instruments]({static}/images/2022-groupes/minor-sing.jpg)]({filename}../2022-groupes/minor-sing.md "Minor Sing")
[![Les 4 membres du groupe en tenues colorées et habillés dans un salon]({static}/images/2022-groupes/odeves.jpg)]({filename}../2022-groupes/odeves.md "Odeves")
[![Les têtes joyeuses des membres du groupe]({static}/images/2022-groupes/dhoad.jpg)]({filename}../2022-groupes/dhoad.md "Dhoad")
[![Dj Click avec plusieurs bras comme shivah]({static}/images/2022-groupes/dj-click.jpg)]({filename}../2022-groupes/dj-click.md "Dj Click")
