AUTHOR = 'Gypsy'
SITENAME = 'Gypsy Lyon Festival'
SITEURL = '.'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = './themes/default/'

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DEFAULT_DATE = 'fs'

# custom Jinja2 filter for category maping
def filter_by_category(articles, category_name):
    return sorted((a for a in articles if a.category == category_name), key=lambda a: a.date, reverse=True)
    
JINJA_FILTERS = {
    "categoryis": filter_by_category,
}

MENU_CATEGORIES = {
        'programmation': 'Programmation',
        'partenariats': 'Partenariats',
        'pratique': 'Gypsy Pratique',
}
STATIC_PATHS = [
    'extra',
]
EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'},
}
